public class MediumSetting implements GraphicsSetting {
    @Override
    public long getNeededProcessingPower() {
        return 500;
    }
}
