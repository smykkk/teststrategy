public class HDSetting implements GraphicsSetting {
    @Override
    public long getNeededProcessingPower() {
        return 1000;
    }
}
