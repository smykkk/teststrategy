public class GraphicsCard {
//    Stwórz klasę GraphicsCard która reprezentuje kartę graficzną.
//    Strategia w tym projekcie będzie służyć do podmieniania w tej klasie obiektu sterownika.
//    Stwórz interfejs sterownika/ustawień karty graficznej.
//    Mechanizm strategii powinien być wykorzystany do podmiany w klasie GraphicsCard obiektu sterownika/ustawienia karty graficznej.
//    Sterownik powinien posiadać metodę:

private GraphicsSetting gpuSetting;

    public GraphicsSetting getGpuSetting() {
        return gpuSetting;
    }

    public void setGpuSetting(GraphicsSetting gpuSetting) {
        this.gpuSetting = gpuSetting;
    }

    public Long getPowerUsingSetting(){
        return gpuSetting.getNeededProcessingPower();
    }
}
