public class LowSetting implements GraphicsSetting {
    @Override
    public long getNeededProcessingPower() {
        return 100;
    }
}