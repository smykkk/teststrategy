public class Main {
    public static void main(String[] args) {
        GraphicsCard gpu1 = new GraphicsCard();

        gpu1.setGpuSetting(new HDSetting());
        System.out.println(gpu1.getPowerUsingSetting());

        gpu1.setGpuSetting(new LowSetting());
        System.out.println(gpu1.getPowerUsingSetting());

        gpu1.setGpuSetting(new MediumSetting());
        System.out.println(gpu1.getPowerUsingSetting());
    }
}
