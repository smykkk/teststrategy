public interface GraphicsSetting {
    long getNeededProcessingPower();
}
